package com.analysis.demo;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * com.analysis.demo
 *
 * @Auther wsj
 * @Date 2024年04月22
 * @PROJECT Behavior Log Tracing
 * @other
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE,ElementType.FIELD})
public @interface PxCustomAnnotation {
}
