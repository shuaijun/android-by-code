package com.analysis.demo;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ASMLogUtil {

    public static final String TAG = "asm_log";
    public static final String CLICK_TAG = "asm_click_log";

    public static void onASMLogPrint(String className, String methodName) {
        Log.d(
                ASMLogUtil.TAG, className + "----" + methodName + "----"
        );
    }

    public static void onASMClickLogPrint(View view, String className) {
        String resourceName = view.getContext().getResources().getResourceEntryName(view.getId());
        String text = "";
        if (view instanceof Button) {
            text = ((Button) view).getText().toString();
        }
        if (view instanceof TextView) {
            text = ((TextView) view).getText().toString();
        }
        Log.d(
                ASMLogUtil.CLICK_TAG,
                className + " ---- onClick ---- " + text + " ---- " + resourceName
        );
    }

}
