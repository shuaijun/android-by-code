```agsl


// 访问类时调用。
// 参数：
//   version: 类的版本。
//   access: 类的访问权限。
//   name: 类的名称。
//   signature: 类的签名（如果存在的话）。
//   superName: 超类的名称（如果有的话）。
//   interfaces: 实现的接口列表。
void visit(int version, int access, String name, String signature, String superName, String[] interfaces);

// 访问类的字段时调用。
// 参数：
//   access: 字段的访问权限。
//   name: 字段的名称。
//   descriptor: 字段的描述符。
//   signature: 字段的签名（如果存在的话）。
//   value: 字段的初始值（如果有的话）。
void visitField(int access, String name, String descriptor, String signature, Object value);

// 访问类的方法时调用。
// 参数：
//   access: 方法的访问权限。
//   name: 方法的名称。
//   descriptor: 方法的描述符。
//   signature: 方法的签名（如果存在的话）。
//   exceptions: 抛出的异常列表。
void visitMethod(int access, String name, String descriptor, String signature, String[] exceptions);

// 访问类的结束标记。
void visitEnd();

// 提供一个子类来覆盖默认的访问控制行为。
// 参数：
//   api: ASM API 的版本。
// 返回值：
//   一个新的 ClassVisitor 实例。
ClassVisitor visitAnnotation(String descriptor, boolean visible);

// 访问类的注释。
// 参数：
//   visible: 是否是用户可见的注释。
//   typeRef: 注释的类型引用。
//   typePath: 注释的类型路径。
//   descriptor: 注释的描述符。
//   value: 注释的值。
void visitAttribute(Attribute attr);

// 访问类的内部类。
// 参数：
//   name: 内部类的名称。
//   outerName: 外部类的名称（如果有的话）。
//   innerName: 内部类的名称（如果是嵌套类的话）。
//   access: 访问权限。
ClassVisitor visitInnerClass(String name, String outerName, String innerName, int access);

// 访问类的外部类。
// 参数：
//   owner: 外部类的名称。
//   name: 方法的名称。
//   descriptor: 方法的描述符。
void visitOuterClass(String owner, String name, String descriptor);

```