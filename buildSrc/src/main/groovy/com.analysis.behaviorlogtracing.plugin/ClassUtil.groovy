package com.analysis.behaviorlogtracing.plugin

public class ClassUtil {

    public static HashSet<String> exclude = new HashSet<>(['android.support',
                                             'androidx',
                                             'android.arch',
                                             'com.google.android',
                                             'android.support.design.widget.TabLayout$ViewPagerOnTabSelectedListener',
                                             'com.google.android.material.tabs.TabLayout$ViewPagerOnTabSelectedListener',
                                             'android.support.v7.app.ActionBarDrawerToggle',
                                             'androidx.appcompat.app.ActionBarDrawerToggle',
                                             'androidx.fragment.app.FragmentActivity',
                                             'androidx.core.app.NotificationManagerCompat',
                                             'androidx.core.app.ComponentActivity',
                                             'android.support.v4.app.NotificationManagerCompat',
                                             'android.support.v4.app.SupportActivity',
                                             'androidx.appcompat.widget.ActionMenuPresenter$OverflowMenuButton',
                                             'android.widget.ActionMenuPresenter$OverflowMenuButton',
                                             'android.support.v7.widget.ActionMenuPresenter$OverflowMenuButton',
                                             'keyboard'])

    public static boolean isJarClassEdit(String className) {
        return className.contains('R$') ||
                className.contains('R2$') ||
                className.contains('R.class') ||
                className.contains('R2.class') ||
                className.contains('BuildConfig.class')
    }

    public static boolean isDirectoryClassEdit(String className) {
        if (exclude.contains(className)) {
            return false
        }
        return true
    }

}