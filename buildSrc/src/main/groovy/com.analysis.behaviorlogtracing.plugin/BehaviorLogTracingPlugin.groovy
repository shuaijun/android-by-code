package com.analysis.behaviorlogtracing.plugin

import com.android.build.gradle.AppExtension
import com.android.build.gradle.BaseExtension
import com.android.build.gradle.LibraryExtension
import org.gradle.api.Plugin
import org.gradle.api.Project


class BehaviorLogTracingPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        BaseExtension extension = project.getExtensions().findByType(AppExtension.class)
        if (extension == null) {
            extension = project.getExtensions().findByType(LibraryExtension.class)
        }

        println("进入 Tracing Plugin")

        extension.registerTransform(new BehaviorLogTracingTransform())
    }

}