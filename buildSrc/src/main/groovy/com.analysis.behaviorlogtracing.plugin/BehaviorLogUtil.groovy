package com.analysis.behaviorlogtracing.plugin;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;

public class BehaviorLogUtil {

    public static byte[] inputToByteSource(InputStream input) throws Exception {
        ByteArrayOutputStream output = null
        try {
            output = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024 * 4]
            int n = 0
            while (-1 != (n = input.read(buffer))) {
                output.write(buffer, 0, n)
            }
            output.flush()
            return output.toByteArray()
        } catch (Exception e) {
            throw e
        } finally {
            IOUtils.closeQuietly(output)
            IOUtils.closeQuietly(input)
        }
    }

    public static boolean isExtendsActivity(String superName) {
        if ("androidx/appcompat/app/AppCompatActivity" == superName
                || "androidx/fragment/app/FragmentActivity" == superName) {
            return true
        }
        return false
    }

    public static boolean isMatchingInterfaces(String[] interfaces, String interfaceName) {
        boolean isMatch = false
        interfaces.each { String inteface ->
            if (inteface == interfaceName) {
                isMatch = true
            }
        }
        return isMatch
    }

}
